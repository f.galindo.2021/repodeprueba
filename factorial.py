def factorial(n):
    result = 1
    for i in range(1, 11):
        result = result * i
        if i == n:
            break
    return result


for i in range(1, 11):
    print(factorial(i))
